﻿using UnityEngine;
using System.Collections;

public class DestroyAfterDelay : MonoBehaviour {

    public float Delay = 1f;

    IEnumerator Start()
    {
        yield return new WaitForSeconds(Delay);
        Destroy(gameObject);
    }
}
