﻿using UnityEngine;
using System.Collections;

public class Entity : MonoBehaviour
{
    public EntityHealthSystem HealthSystem;
    public Transform DeathPrefab, HealthChangePrefab;

    public AudioSource ObjectDestroyed;

    public float ScaleMin = 1, ScaleMax = 1;

    void Start()
    {
        HealthSystem.OnHealthChangedEvent += OnHealthChanged;
        HealthSystem.OnDeathEvent += OnDeath;

        transform.localScale = Vector3.one * Random.Range(ScaleMin, ScaleMax);
    }

    private void OnDeath()
    {
        if (DeathPrefab) Instantiate(DeathPrefab, transform.position, Quaternion.identity);

        Ondestruction();
        Destroy(gameObject);
    }

    private void OnHealthChanged(int value)
    {
        if (HealthChangePrefab) Instantiate(HealthChangePrefab, transform.position, Quaternion.identity);
    }

    private void Ondestruction()
    {
        ObjectDestroyed.transform.parent = null;
        ObjectDestroyed.Play();
    }
}
