﻿using UnityEngine;
using System.Collections;

public class ObstacleMoveForward : MonoBehaviour
{

    public Rigidbody Rigidbody;
    private Vector3 direction;

    public float speed = 10f;

    // Use this for initialization
    void Start()
    {
        direction = Random.onUnitSphere;
    }

    // Update is called once per frame
    void Update()
    {

        transform.rotation = Quaternion.LookRotation(direction, transform.up);
        Rigidbody.MovePosition(transform.position + direction * speed * Time.deltaTime);


    }
}
