﻿using UnityEngine;
using System.Collections;

public class DestroyWhenNotVisible : MonoBehaviour
{
    public Collider Collider;
    public float DestroyPosOffset = 100;
	
	private void Update ()
    {
        var point = transform.position.z;
        if (Collider) point = Collider.bounds.max.z;

        if (point < GameController.I.WorldEndPositionZ - DestroyPosOffset)
        {
            Destroy(gameObject);
        }
    }
}
