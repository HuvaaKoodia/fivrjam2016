﻿using UnityEngine;
using System.Collections;

public class ObstacleRotate : MonoBehaviour
{

    public float speed = 10f;
    public Vector3 Axis = Vector3.up;
    public Space Space = Space.World;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Axis, speed * Time.deltaTime, Space);

    }
}
