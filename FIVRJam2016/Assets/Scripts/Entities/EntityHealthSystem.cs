﻿using UnityEngine;

public class EntityHealthSystem : MonoBehaviour
{
    public delegate void IntEvent(int value);

    public int MaxHealth = 100;

    public event IntEvent OnHealthChangedEvent;
    public event System.Action OnDeathEvent;

    private int mHullHealth;

    public int HullHealth
    {
        get { return mHullHealth; }
    }

    public void AddDamage(int damage)
    {
        if (mHullHealth == 0) return;

        mHullHealth -= damage;

        if (mHullHealth <= 0)
        {
            if (OnDeathEvent != null)
                OnDeathEvent();
            return;
        }

        if (OnHealthChangedEvent != null) OnHealthChangedEvent(damage);
    }

    public void AddHealth(int health)
    {
        mHullHealth += health;
        if (mHullHealth > MaxHealth)
            mHullHealth = MaxHealth;

        if (OnHealthChangedEvent != null) OnHealthChangedEvent(health);
    }

    private void Awake()
    {
        mHullHealth = MaxHealth;
    }
}
