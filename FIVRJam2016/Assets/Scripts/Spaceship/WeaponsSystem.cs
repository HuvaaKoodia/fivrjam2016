﻿using UnityEngine;

public class WeaponsSystem : MonoBehaviour
{
    public event System.Action OnShootLaserEvent;
    public event System.Action OnShootMissileEvent;

    public SpaceshipHealth SpaceshipHealth;
    public Rigidbody Rigidbody;

    public Bullet BulletPrefab;
    public Missile MissilePrefab;

    public float BulletCooldown = 0.1f, MissileCooldown = 4f;
    public float BulletConditionDelayMax = 2f, MissileConditionDelayMax = 2f;
    private float bulletDelay, missileDelay;

    public Transform BulletShootPosition, MissileShootPosition, AimPoint;

    private void Start()
    {
        SpaceshipHealth.OnDeath += OnDeath;
    }

    private void OnDeath(SpaceshipHealth.HealthType type)
    {
        if (type == SpaceshipHealth.HealthType.Hull)
            enabled = false;
    }

    public void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            if (bulletDelay < Time.time)
            {
                float condition = SpaceshipHealth.GetHealth(SpaceshipHealth.HealthType.LeftGun) / (float)SpaceshipHealth.MaxHealth;

                if (condition > 0)
                {
                    bulletDelay = Time.time + BulletCooldown + (1 - condition) * BulletConditionDelayMax;
                    var bullet = Instantiate(BulletPrefab, BulletShootPosition.position, Quaternion.identity) as Bullet;
                    bullet.Init(GetAimDirection(BulletShootPosition, true, condition), Rigidbody.velocity);

                    OnShootLaserEvent();
                }
            }
        }

        if (Input.GetButton("Fire2"))
        {
            if (missileDelay < Time.time)
            {
                float condition = SpaceshipHealth.GetHealth(SpaceshipHealth.HealthType.RightGun) / (float)SpaceshipHealth.MaxHealth;

                if (condition > 0)
                {
                    missileDelay = Time.time + MissileCooldown + (1 - condition) * MissileConditionDelayMax;
                    var missile = Instantiate(MissilePrefab, MissileShootPosition.position, Quaternion.identity) as Missile;
                    missile.Init(GetAimDirection(MissileShootPosition, true, condition), Rigidbody.velocity);

                    OnShootMissileEvent();
                }
            }
        }
    }

    private Vector3 GetAimDirection(Transform shootPosition, bool randomize = false, float condition = 1)
    {
        var direction = (AimPoint.position - shootPosition.position).normalized;

        if (randomize)
        {
            direction = Quaternion.AngleAxis(Random.Range(-0.5f - 2 * (1f - condition), 0.5f * 2 * (1f - condition)), Vector3.up) * direction;
        }

        return direction;
    }
}
