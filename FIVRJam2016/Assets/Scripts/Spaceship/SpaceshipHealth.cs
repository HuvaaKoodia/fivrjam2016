﻿using UnityEngine;

public class SpaceshipHealth : MonoBehaviour
{
    public enum HealthType
    {
        Hull = 0,
        LeftMotor,
        RightMotor,
        LeftGun,
        RightGun,
        _Amount
    }

    public Rigidbody Rigidbody;

    public delegate void OnHealthChangeDelegate(HealthType healthType, int change);
    public delegate void OnDeathDelegate(HealthType healthType);

    public int MaxHealth = 100;

    public event OnHealthChangeDelegate OnHealthChange = delegate { };
    public event OnDeathDelegate OnDeath = delegate { };

    private int[] healthArray;

    private bool mDead = false;

    public bool IsDead
    {
        get { return mDead; }
    }

    public int GetHealth(HealthType type)
    {
        return healthArray[(int) type];
    }

    private void Awake()
    {
        healthArray = new int[(int)HealthType._Amount];
        for (int i = 0; i < healthArray.Length; i++)
        {
            healthArray[i] = MaxHealth;
        }
    }

    public void AddDamage(HealthType type, int damage)
    {
        healthArray[(int)type] -= damage;
        if (healthArray[(int)type] <= 0)
        {
            OnDeath(type);
            mDead = true;
        }

        OnHealthChange(type, -damage);
    }

    public void AddHealth(HealthType type, int health)
    {
        healthArray[(int)type] += health;
        if (healthArray[(int)type] > MaxHealth)
            healthArray[(int)type] = MaxHealth;

        OnHealthChange(type, health);
    }

}
