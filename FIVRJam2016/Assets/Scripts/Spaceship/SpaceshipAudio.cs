﻿using UnityEngine;
using System.Collections;
using System;

public class SpaceshipAudio : MonoBehaviour {

    public AudioSource HullHitSound;
    public AudioPlayer Firelaser;
    public AudioSource FireMissile;
    public AudioSource FixShip;

    public AudioSource HullDamageWarning;
    public AudioSource LeftEngineWarning;
    public AudioSource RightEngineWarning;
    public AudioSource LaserMalfunctionWarning;
    public AudioSource MissileMalfunctionWarning;
    
    public SpaceshipHealth SpaceshipHealth;
    public WeaponsSystem WeaponsSystem;

    public UIDamageIndicator HullDamageIndicator;
    public UIDamageIndicator LeftEngineDamageIndicator;
    public UIDamageIndicator RightEngineDamageIndicator;
    public UIDamageIndicator LaserDamageIndicator;
    public UIDamageIndicator MissileDamageIndicator;



	// Use this for initialization
	void Start () {
        SpaceshipHealth.OnHealthChange += HealthChange;
        SpaceshipHealth.OnDeath += OnDeath;

        WeaponsSystem.OnShootLaserEvent += OnShootLaser;

        WeaponsSystem.OnShootMissileEvent += OnShootMissile;

        HullDamageIndicator.OnWarningEvent += OnHullWarning;
        LeftEngineDamageIndicator.OnWarningEvent += OnLEWarningEvent;
        RightEngineDamageIndicator.OnWarningEvent += OnREWaningEvent;
        LaserDamageIndicator.OnWarningEvent += OnLaserMalfunction;
        MissileDamageIndicator.OnWarningEvent += OnMissileMalfunction;

        HullDamageIndicator.OnOkayEvent += OnHullOK;
        LeftEngineDamageIndicator.OnOkayEvent += OnLEOK;
        RightEngineDamageIndicator.OnOkayEvent += OnREOK;
        LaserDamageIndicator.OnOkayEvent += OnLaserOK;
        MissileDamageIndicator.OnOkayEvent += OnMissileOK;
    }

    private void OnMissileMalfunction()
    {
        MissileMalfunctionWarning.Play();
    }

    private void OnREWaningEvent()
    {
        RightEngineWarning.Play();
    }

    private void OnLEWarningEvent()
    {
        LeftEngineWarning.Play();
    }

    private void OnLaserMalfunction()
    {
        LaserMalfunctionWarning.Play();
    }

    private void OnHullWarning()
    {
        HullDamageWarning.Play();
    }

    private void OnMissileOK()
    {
        MissileMalfunctionWarning.Stop();
    }

    private void OnREOK()
    {
        RightEngineWarning.Stop();
    }

    private void OnLEOK()
    {
        LeftEngineWarning.Stop();
    }

    private void OnHullOK()
    {
        HullDamageWarning.Stop();
    }

    private void OnLaserOK()
    {
        LaserMalfunctionWarning.Stop();
    }

    private void OnShootMissile()
    {
        FireMissile.Play();
    }

    private void OnShootLaser()
    {
        Firelaser.Play();
    }


    private void HealthChange(SpaceshipHealth.HealthType healthType, int change)
    {
        if (change < 0)
        {
            HullHitSound.Play();
        } if (change > 0)
        {
            FixShip.Play();
        }
    }

    void OnDeath(SpaceshipHealth.HealthType healthType)
    {
        if (healthType != SpaceshipHealth.HealthType.Hull) return;

        MissileMalfunctionWarning.volume = 0;
        RightEngineWarning.volume = 0;
        LeftEngineWarning.volume = 0;
        HullDamageWarning.volume = 0;
        LaserMalfunctionWarning.volume = 0;
    }

    // Update is called once per frame
    void Update () {
	
	}
}
