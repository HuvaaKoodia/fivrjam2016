﻿using System.Collections.Generic;
using UnityEngine;

public class SpaceshipSystems : MonoBehaviour
{
    public enum SystemID
    {
        Center = 0,
        Left,
        Right,
        _Amount
    }

    public delegate void SystemEvent(SystemID id, bool active);

    public SystemEvent OnSystemStateChanged;
    public SpaceshipHealth HealthSystem;

    private  bool[] screenActiveState;

    private void Start()
    {
        HealthSystem.OnHealthChange += OnHealthChange;
        HealthSystem.OnDeath += OnDeath;
        screenActiveState = new bool[(int)SystemID._Amount];

        for (int i = 0; i < (int)SystemID._Amount; i++)
        {
            screenActiveState[i] = true;
        }
    }

    private void OnDeath(SpaceshipHealth.HealthType healthType)
    {
        //disable all on death
        for (int i = 0; i < (int)SystemID._Amount; i++)
        {
            SetActive((SystemID)i, false);
        }
    }

    private void OnHealthChange(SpaceshipHealth.HealthType healthType, int change)
    {
        if (healthType == SpaceshipHealth.HealthType.Hull)
        {
            if (change < 0)
            {
                //deactivate one random screen
                if (Random.Range(0, 100) < 50)
                {
                    var list = GetScreens(true);

                    if (list.Count > 0)
                    {
                        var systemIndex = list[Random.Range(0, list.Count)];
                        SetActive((SystemID)systemIndex, false);
                    }
                }
            }
        }

        if (change > 0)
        {
            //fix one random screen
            var list = GetScreens(false);

            if (list.Count > 0)
            {
                var systemIndex = list[Random.Range(0, list.Count)];
                SetActive((SystemID)systemIndex, true);
            }
        }
    }

    private void SetActive(SystemID id, bool active)
    {
        screenActiveState[(int)id] = active;
        OnSystemStateChanged(id, active);
    }

    private List<int> GetScreens(bool active)
    {
        var list = new List<int>();

        for (int i = 0; i < (int)SystemID._Amount; i++)
        {
            if (screenActiveState[i] == active)
            {
                list.Add(i);
            }
        }
        return list;
    }
}
