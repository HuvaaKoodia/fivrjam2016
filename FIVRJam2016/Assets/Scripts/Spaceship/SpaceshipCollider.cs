﻿using UnityEngine;
using System.Collections;

public class SpaceshipCollider : MonoBehaviour
{
    public SpaceshipHealth.HealthType ColliderHealthType = SpaceshipHealth.HealthType.Hull;
    public float ImpactDamageMultiplier = 1f;

    private SpaceshipHealth mSpaceshipHealth;

    void Start()
    {
        mSpaceshipHealth = GetComponentInParent<SpaceshipHealth>();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!enabled) return;

        var impactDamage = mSpaceshipHealth.Rigidbody.velocity.magnitude * ImpactDamageMultiplier;
        mSpaceshipHealth.AddDamage(ColliderHealthType, (int)impactDamage);
    }
}