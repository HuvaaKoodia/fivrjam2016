﻿using UnityEngine;
using System.Collections;

public class SpaceshipFuel : MonoBehaviour
{
    public int MaxFuel = 100;
    public int MaxBoostConsumption = 20;
    public int MaxBrakeConsumption = 20;

    private float mFuel;

    private Spaceship mSpaceship;

    public float CurrentFuel
    {
        get
        {
            return mFuel;
        }
    }

    public bool IsEmpty
    {
        get { return mFuel <= 0; }
    }

    public void Refill()
    {
        mFuel = MaxFuel;
    }

    void Start()
    {
        mFuel = MaxFuel;
        mSpaceship = GetComponent<Spaceship>();
    }

    void Update()
    {
        if (mSpaceship.ForwardSpeed < mSpaceship.DefaultForwardVelocity-1)
        {
            var consumption = (mSpaceship.DefaultForwardVelocity - mSpaceship.ForwardSpeed) / (mSpaceship.DefaultForwardVelocity - mSpaceship.MinForwardVelocity) * MaxBrakeConsumption;
            mFuel -= (consumption * Time.deltaTime);
            if (mFuel < 0)
                mFuel = 0;
        }
        else if (mSpaceship.ForwardSpeed > mSpaceship.DefaultForwardVelocity+1)
        {
            var consumption = (mSpaceship.ForwardSpeed - mSpaceship.DefaultForwardVelocity) / (mSpaceship.MaxForwardVelocity - mSpaceship.DefaultForwardVelocity) * MaxBoostConsumption;
            mFuel -= (consumption * Time.deltaTime);
            if (mFuel < 0)
                mFuel = 0;
        }
    }
}
