﻿using UnityEngine;
using System.Collections;
using UnityEngine.VR;
using UnityEngine.UI;
using System;

public class Spaceship : MonoBehaviour
{
    public float MinForwardVelocity = 10f, DefaultForwardVelocity = 50f, MaxForwardVelocity = 100f;
    public float ForwardAccelerationForce = 10f, BrakeForce = 5f;

    public float MaxHorizontalVelocity = 10f, MaxVerticalVelocity = 100f;
    public float HorizontalAccelerationForce = 10f, VerticalAccelerationForce = 10f;
    public float HorizontalBrakeForce = 10f, VerticalBrakeForce = 10f;
    public float MaxRollVelocity = 10f, RollForce = 10f;

    public SpaceshipHealth SpaceshipHealth;
    public Rigidbody Rigidbody;
    private float startTime;

    private float mTargetForwardVelocity;

    private SpaceshipFuel mSpaceshipFuel;

    public float ForwardSpeed
    {
        get
        {
            return mTargetForwardVelocity;
        }
    }

    private void Start()
    {
        startTime = Time.time;
        mTargetForwardVelocity = DefaultForwardVelocity;
        mSpaceshipFuel = GetComponent<SpaceshipFuel>();

        SpaceshipHealth.OnDeath += OnDeath;
        SpaceshipHealth.OnHealthChange += OnHealthChange;
    }

    private void OnHealthChange(SpaceshipHealth.HealthType healthType, int change)
    {
        if (change < 0)
        {
            //ShakeCamera(-change * HitCameraShakeMultiplier);
            //GameController.I.VibrateHandheld();
        }
    }

    private void OnDeath(SpaceshipHealth.HealthType type)
    {
        if (type == SpaceshipHealth.HealthType.Hull)
            enabled = false;
    }

    private void FixedUpdate()
    {
        //movement input
        float horizontalAxis = Input.GetAxis("Horizontal");
        float verticalAxis = Input.GetAxis("Vertical");

        if (GameController.InvertMovementY) verticalAxis = -verticalAxis;

        float rollAxis = Input.GetAxis("RollAxis");
        float acceleration = !mSpaceshipFuel.IsEmpty ? Input.GetAxis("Acceleration") : 0;

        //engine condition hacks!
        float conditionRight = Mathf.Max(0.1f, (SpaceshipHealth.GetHealth(SpaceshipHealth.HealthType.RightMotor) / (float)SpaceshipHealth.MaxHealth));
        float conditionLeft = Mathf.Max(0.1f, (SpaceshipHealth.GetHealth(SpaceshipHealth.HealthType.LeftMotor) / (float)SpaceshipHealth.MaxHealth));

        float rollCondition = Mathf.Max(0.1f, conditionLeft * conditionRight);

        float currentHorizontalVelocity = Vector3.Dot(Rigidbody.velocity, transform.right);
        float currentVerticalVelocity = Vector3.Dot(Rigidbody.velocity, transform.up);

        //uses brake force instead of acceleration when changing direction
        float horizontalAccelerationForce = HorizontalAccelerationForce;
        if (Mathf.Sign(currentHorizontalVelocity) != Mathf.Sign(horizontalAxis))
            horizontalAccelerationForce = HorizontalBrakeForce;

        //horizontal acceleration
        Rigidbody.AddForce(transform.right * horizontalAxis * horizontalAccelerationForce * conditionRight, ForceMode.Acceleration);

        //uses brake force instead of acceleration when changing direction
        float verticalAccelerationForce = VerticalAccelerationForce;
        if (Mathf.Sign(currentVerticalVelocity) != Mathf.Sign(verticalAxis))
            verticalAccelerationForce = VerticalBrakeForce;

        //vertical acceleration
        Rigidbody.AddForce(transform.up * verticalAxis * verticalAccelerationForce * conditionLeft, ForceMode.Acceleration);

        if (Rigidbody.angularVelocity.magnitude < MaxRollVelocity)
            Rigidbody.AddTorque(Vector3.forward * rollAxis * RollForce * rollCondition, ForceMode.Acceleration);

        //movement axis stabilization
        if (horizontalAxis == 0)
        {
            float velocityAbs = Mathf.Abs(currentHorizontalVelocity);
            float velocitySign = Mathf.Sign(currentHorizontalVelocity);
            float stabilizationForce = Mathf.Min(HorizontalAccelerationForce, velocityAbs);

            Rigidbody.AddForce(transform.right * -velocitySign * stabilizationForce * conditionRight, ForceMode.Acceleration);
        }

        if (verticalAxis == 0)
        {
            float velocityAbs = Mathf.Abs(currentVerticalVelocity);
            float velocitySign = Mathf.Sign(currentVerticalVelocity);
            float stabilizationForce = Mathf.Min(HorizontalAccelerationForce, velocityAbs);

            Rigidbody.AddForce(transform.up * -velocitySign * stabilizationForce * conditionLeft, ForceMode.Acceleration);
        }

        if (rollAxis == 0)
        {
            var rollVelocity = Vector3.Dot(Rigidbody.angularVelocity, transform.forward);
            if (rollVelocity != 0)
                Rigidbody.AddTorque(-transform.forward * rollVelocity * RollForce *rollCondition, ForceMode.Acceleration);
        }

        if (acceleration > 0 && mTargetForwardVelocity < MaxForwardVelocity)
        {
            mTargetForwardVelocity += ForwardAccelerationForce * acceleration * Time.deltaTime;
        }
        else if (acceleration < 0 && mTargetForwardVelocity > MinForwardVelocity)
        {
            mTargetForwardVelocity += BrakeForce * acceleration * Time.deltaTime;
        }
        else if (acceleration == 0)
        {
            if (mTargetForwardVelocity < DefaultForwardVelocity)
            {
                mTargetForwardVelocity += Mathf.Min(ForwardAccelerationForce * Time.deltaTime, DefaultForwardVelocity- mTargetForwardVelocity);
            }
            else if (mTargetForwardVelocity > DefaultForwardVelocity)
            {
                mTargetForwardVelocity -= Mathf.Min(BrakeForce * Time.deltaTime, mTargetForwardVelocity - DefaultForwardVelocity);
            }
        }

        Rigidbody.velocity = new Vector3(Rigidbody.velocity.x, Rigidbody.velocity.y, mTargetForwardVelocity);
    }

    //camera shake quick hack, no good in VR

    /*public Transform CameraShakeOffset;
    public float HitCameraShakeMultiplier = 1, CameraShakeDampMultiplier = 1;
    private float shakeForce, targetShakeForce;

    private void Update()
    {
        shakeForce = Mathf.Lerp(shakeForce, targetShakeForce, Time.deltaTime * CameraShakeDampMultiplier);
        CameraShakeOffset.localPosition = UnityEngine.Random.insideUnitSphere * shakeForce;
    }

    public void ShakeCamera(float force)
    {
        shakeForce = force;
        targetShakeForce = 0;
    }*/
}
