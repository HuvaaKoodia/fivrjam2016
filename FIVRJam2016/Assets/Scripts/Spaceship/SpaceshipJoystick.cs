﻿using UnityEngine;
using System.Collections;

public class SpaceshipJoystick : MonoBehaviour
{
    public string VerticalInputAxis = "Vertical";
    public float MaxVerticalAngle = 45f;

    public string HorizontalInputAxis = "Horizontal";
    public float MaxHorizontalAngle = 45f;

    private Vector3 mOriginalRotation;

    void Start()
    {
        mOriginalRotation = transform.localEulerAngles;
    }

    void Update()
    {
        var vertical = -Input.GetAxis(VerticalInputAxis);
        var horizontal = -Input.GetAxis(HorizontalInputAxis);

        Vector3 rotation;
        rotation.x = mOriginalRotation.x + (vertical * MaxVerticalAngle);
        rotation.y = mOriginalRotation.y;
        rotation.z = mOriginalRotation.z + (horizontal * MaxVerticalAngle);
        transform.localEulerAngles = rotation;
    }
}