﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.Events;

public class SpaceshipScreen : MonoBehaviour
{
    public SpaceshipSystems.SystemID ScreenID;

	public SpaceshipSystems SpaceShipScreenSystem;

    public UnityEvent OnScreenEnabled;
    public UnityEvent OnScreenDisabled;

    private void Start()
    {
        SpaceShipScreenSystem.OnSystemStateChanged += OnStateChanged;
    }

    private void OnStateChanged(SpaceshipSystems.SystemID id, bool active)
    {
        if (id == ScreenID)
        {
            if (active)
                OnScreenEnabled.Invoke();
            else
                OnScreenDisabled.Invoke();
        }
    }
}
