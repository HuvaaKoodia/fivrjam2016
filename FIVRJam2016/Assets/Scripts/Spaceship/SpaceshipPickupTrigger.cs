﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class SpaceshipPickupTrigger : MonoBehaviour
{
    public string RepairPickupTag = "RepairPickup";
    public string FuelPickupTag = "FuelPickup";

    public Spaceship Spaceship;

    public UnityEvent OnRepairPickup;
    public UnityEvent OnFuelPickup;

    private SpaceshipHealth mSpaceshipHealth;
    private SpaceshipFuel mSpaceshipFuel;

    void Start()
    {
        mSpaceshipHealth = Spaceship.GetComponent<SpaceshipHealth>();
        mSpaceshipFuel = Spaceship.GetComponent<SpaceshipFuel>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == RepairPickupTag)
        {
            // heal
            int minIndex = -1, minHealth = 100;
            for (int i = 0; i < (int)SpaceshipHealth.HealthType._Amount; i++)
            {
                int health = mSpaceshipHealth.GetHealth((SpaceshipHealth.HealthType)i);

                if (health < minHealth)
                {
                    minHealth = health;
                    minIndex = i;
                }
            }
            if (minIndex != -1)
            {
                mSpaceshipHealth.AddHealth((SpaceshipHealth.HealthType)minIndex, 50);
            }
            OnRepairPickup.Invoke();
            Destroy(other.gameObject);
        }
        else if (other.gameObject.tag == FuelPickupTag)
        {
            mSpaceshipFuel.Refill();
            OnFuelPickup.Invoke();
            Destroy(other.gameObject);
        }
    }
}