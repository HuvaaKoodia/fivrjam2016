﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController I;
    public Spawner Spawner;
    public Spaceship Spaceship;


    public static bool InvertMovementY = false;

    public GameObject InstructionScreen;

    public float WorldEndPositionZ { get; internal set; }

    private void Awake()
    {
        I = this;

        Application.targetFrameRate = 90;
        Time.timeScale = 0;

        //Important VR commands

        //supersampling
        //VRSettings.renderScale = 1.4f;

        //reset head position
        //InputTracking.Recenter();

        //turns of the flat screen view
        //VRSettings.showDeviceView = false;
    }

    private void Start()
    {
        Spawner.StartObstacleSpawner();
    }

    private void Update()
    {
        if (Time.timeScale == 0) Spaceship.transform.position = Vector3.zero;

        if (Input.GetButtonDown("InvertYAxis"))
        {
            InvertMovementY = !InvertMovementY;
        }
        

        if (Input.GetButtonDown("Reset"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (Time.timeScale == 0 && Input.GetAxisRaw("Acceleration") > 0)
        {
            Time.timeScale = 1f;
            InstructionScreen.SetActive(false);
        }

        if (Spaceship != null)
            WorldEndPositionZ = Spaceship.transform.position.z;
        else
            WorldEndPositionZ = 0;
    }

}
