﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour
{
    public Spaceship Spaceship;

    private float mScore = 0;
    private SpaceshipHealth mSpaceshipHealth;

    public int CurrentScore
    {
        get { return (int)mScore; }
    }

    void Start()
    {
        mSpaceshipHealth = Spaceship.GetComponent<SpaceshipHealth>();
    }

    void Update()
    {
        if (!mSpaceshipHealth.IsDead)
            mScore += (Spaceship.ForwardSpeed / 10) * Time.deltaTime;
        //print(mScore);
    }
}