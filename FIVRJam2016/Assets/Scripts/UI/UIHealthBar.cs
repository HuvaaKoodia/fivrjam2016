﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIHealthBar: MonoBehaviour
{
    public SpaceshipHealth SpaceshipHealth;

    public SpaceshipHealth.HealthType HealthType;

    public Image FillSprite;

    void Start()
    {
        UpdateFill();
        SpaceshipHealth.OnHealthChange += OnHealthChange;
    }

    void OnHealthChange(SpaceshipHealth.HealthType healthType, int change)
    {
        if (healthType == HealthType)
        {
            UpdateFill();
        }
    }

    private void UpdateFill()
    {
        FillSprite.fillAmount = 1 - (float)SpaceshipHealth.GetHealth(HealthType) / (float)SpaceshipHealth.MaxHealth;
    }
}
