﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIDamageIndicator : MonoBehaviour
{
    public int WarningMaxHealth = 50;
    public float WarningFlashInterval = 3f;
    public Color WarningColor = Color.yellow;

    public int CriticalMaxHealth = 25;
    public float CriticalFlashInterval = 6f;
    public Color CriticalColor = Color.red;

    public Image ColorTarget;

    public SpaceshipHealth.HealthType HealthType;

    public SpaceshipHealth SpaceshipHealth;

    private CanvasGroup mCanvasGroup;

    private bool mFlash = false;
    private float mFlashInterval;

    public event System.Action OnWarningEvent, OnCriticalEvent, OnOkayEvent;

    // Use this for initialization
    void Start()
    {
        SpaceshipHealth.OnHealthChange += HealthChange;
        mCanvasGroup = GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        if (mFlash)
        {
            mCanvasGroup.alpha = (Mathf.Sin(Time.time * mFlashInterval) + 1) / 2f;
        }
        else
        {
            mCanvasGroup.alpha = 0f;
        }
    }

    void HealthChange(SpaceshipHealth.HealthType healthType, int change)
    {
        if (healthType == HealthType)
        {
            float health = SpaceshipHealth.GetHealth(HealthType);
            if (health <= CriticalMaxHealth)
            {
                mFlash = true;
                mFlashInterval = CriticalFlashInterval;
                ColorTarget.color = CriticalColor;
            }
            else if (health <= WarningMaxHealth)
            {
                mFlash = true;
                mFlashInterval = WarningFlashInterval;
                ColorTarget.color = WarningColor;
                OnWarningEvent();
                
            }
            else
            {
                mFlash = false;
                OnOkayEvent();
            }
        }
    }
}