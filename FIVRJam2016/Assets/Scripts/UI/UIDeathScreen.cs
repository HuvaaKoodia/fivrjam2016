﻿using UnityEngine;
using System.Collections;

public class UIDeathScreen : MonoBehaviour
{
    public SpaceshipHealth SpaceshipHealth;

    public GameObject Enable;

    void Start()
    {
        SpaceshipHealth.OnDeath += OnSpaceshipDeath;
    }

    void OnSpaceshipDeath(SpaceshipHealth.HealthType type)
    {
        if (type == SpaceshipHealth.HealthType.Hull)
        {
            Enable.SetActive(true);
        }
    }
}