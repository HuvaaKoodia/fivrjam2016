﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UISpeedIndicator : MonoBehaviour
{
    public Spaceship Spaceship;

    public Text Text;

    void Update()
    {
        Text.text = Mathf.RoundToInt(Spaceship.ForwardSpeed).ToString();
    }
}