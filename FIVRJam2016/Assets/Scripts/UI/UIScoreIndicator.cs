﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIScoreIndicator : MonoBehaviour
{
    public Score ScoreSystem;

    private Text mText;

    void Start()
    {
        mText = GetComponent<Text>();
    }

    void Update()
    {
        mText.text = ScoreSystem.CurrentScore.ToString();
    }
}