﻿using UnityEngine;
using UnityEngine.UI;

public class UIFuelBar : MonoBehaviour
{
    public SpaceshipFuel SpaceshipFuel;

    public AnimationCurve FuelIndexWeight;
    public Image[] Images;
    private Slider mSlider;

    void Start()
    {
        
    }

    void Update()
    {
        //quick dirty hack!
        float percent = (float)SpaceshipFuel.CurrentFuel / (float)SpaceshipFuel.MaxFuel;

        int index = (int)(FuelIndexWeight.Evaluate(percent) * Images.Length);

        for (int i = 0; i < Images.Length; i++)
        {
            Images[(Images.Length - 1) - i].enabled = i < index;
        }
    }
}
