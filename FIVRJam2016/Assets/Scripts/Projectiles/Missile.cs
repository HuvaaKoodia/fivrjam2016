﻿using UnityEngine;

public class Missile : MonoBehaviour
{
    public Rigidbody Rigidbody;
    public int Damage = 10;
    public float Speed = 10f;
    public float ExplosionRadius = 10f;

    public Transform ExplosionPrefab;

    public void Init(Vector3 direction, Vector3 startVelocity)
    {
        Rigidbody.velocity = startVelocity + direction * Speed;
    }

    private void OnTriggerEnter(Collider collider)
    {
        Destroy(gameObject);
        Instantiate(ExplosionPrefab, transform.position, Quaternion.identity);

        var collisions = Physics.OverlapSphere(transform.position, ExplosionRadius, 1 << LayerMask.NameToLayer("Obstacle"));

        for (int i = 0; i < collisions.Length; i++)
        {
            var healthSystem = collisions[i].GetComponent<EntityHealthSystem>();
            if (healthSystem) healthSystem.AddDamage(Damage);
        }
    }
}