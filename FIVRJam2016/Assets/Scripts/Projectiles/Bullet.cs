﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Rigidbody Rigidbody;
    public int Damage = 10;
    public float Speed = 10f;

    public void Init(Vector3 direction, Vector3 startVelocity)
    {
        Rigidbody.velocity = startVelocity + direction * Speed;
    }

    private void OnTriggerEnter(Collider collider)
    {
        Destroy(gameObject);

        var healthSystem = collider.GetComponent<EntityHealthSystem>();
        if (healthSystem == null) return;
        healthSystem.AddDamage(Damage);
    }
}
