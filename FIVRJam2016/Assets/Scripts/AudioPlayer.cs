﻿using UnityEngine;
using System.Collections;

public class AudioPlayer : MonoBehaviour {

    public AudioSource[] AudioSources;

    private int index;

	public void Play()
    {
        AudioSources[index].Play();
        index++;
        if (index == AudioSources.Length)
        {
            index = 0;
        }
    }
}
