﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class SongStartingPoint : MonoBehaviour {

    public AudioSource audio;

    // Use this for initialization
    void Start () {
        audio.time = Random.Range(0,audio.clip.length);
    }

}
