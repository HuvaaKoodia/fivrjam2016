﻿using System.Collections;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [System.Serializable]
    public class SpawnObjectStats
    {
        public GameObject[] Prefabs;
        public Vector2 SpawnDelayStart, SpawnDelayEnd, SpawnAreaSize;
        public float SpawnAreaDistance = 100f;
        public AnimationCurve SpawnDelayTimeCurve;
        public float SpawnDelayEndTime = 10f;
        public Vector2 SpawnAmountStart = Vector2.one, SpawnAmountEnd = Vector2.one;
    }

    public SpawnObjectStats ObstacleStats;
    public SpawnObjectStats StructureStats;
    public SpawnObjectStats PowerUpStats;

    void Start()
    {

    }

    public void StartObstacleSpawner()
    {
        if (!enabled) return;

        StartCoroutine(SpawnerCoroutine(ObstacleStats));
        StartCoroutine(SpawnerCoroutine(StructureStats));
        StartCoroutine(SpawnerCoroutine(PowerUpStats));
    }

    private IEnumerator SpawnerCoroutine(SpawnObjectStats stats)
    {
        float startTime = Time.time;
        var spaceShip = GameController.I.Spaceship;

        while (true)
        {
            float speedMultiplier = 1;

            float time = (Time.time - startTime) / stats.SpawnDelayEndTime;

            float delayMin = Mathf.Lerp(stats.SpawnDelayStart.x, stats.SpawnDelayEnd.x, stats.SpawnDelayTimeCurve.Evaluate(time));
            float delayMax = Mathf.Lerp(stats.SpawnDelayStart.y, stats.SpawnDelayEnd.y, stats.SpawnDelayTimeCurve.Evaluate(time));

            float delay = Random.Range(delayMin, delayMax);

            while (delay > 0)
            {
                float velocity = Mathf.Max(spaceShip.MinForwardVelocity, spaceShip.Rigidbody.velocity.z);

                speedMultiplier = velocity / spaceShip.MaxForwardVelocity;
                delay -= Time.deltaTime * speedMultiplier;
                yield return null;
            }

            yield return null;

            float amountMin = Mathf.Lerp(stats.SpawnAmountStart.x, stats.SpawnAmountEnd.x, stats.SpawnDelayTimeCurve.Evaluate(startTime / stats.SpawnDelayEndTime));
            float amountMax = Mathf.Lerp(stats.SpawnAmountStart.y, stats.SpawnAmountEnd.y, stats.SpawnDelayTimeCurve.Evaluate(startTime / stats.SpawnDelayEndTime));

            int amount = (int)Mathf.Round(Random.Range(amountMin, amountMax));

            for (int i = 0; i < amount; i++)
            {
                var position = GameController.I.Spaceship.transform.position + Vector3.forward * stats.SpawnAreaDistance;
                position += new Vector3(Random.Range(-stats.SpawnAreaSize.x, stats.SpawnAreaSize.x), Random.Range(-stats.SpawnAreaSize.y, stats.SpawnAreaSize.y), 0);

                var prefab = stats.Prefabs[Random.Range(0, stats.Prefabs.Length)];
                Instantiate(prefab, position, Quaternion.identity);

                yield return new WaitForSeconds(Random.Range(0.1f, 0.3f));
            }
        }
    }
}
